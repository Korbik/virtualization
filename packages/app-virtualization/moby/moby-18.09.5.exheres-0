# Copyright 2016 Marc-Antoine Perennou <marc-antoine.perennou@clever-cloud.com>
# Copyright 2017 Arnaud Lefebvre <arnaud.lefebvre@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

# -ce because Moby now has a Community Edition tag
MY_PV=${PV}-ce
MY_PNV=${PN}-${MY_PV}
# Most of the time, files or binaries will be called docker- and not moby
MY_DPN=docker
MY_DPNV=${MY_DPN}-${MY_PV}

require github [ user=${MY_DPN} pn=${MY_DPN}-ce tag=v${PV} ]
require systemd-service [ systemd_files=[ components/engine/contrib/init/systemd ] ]
require openrc-service
require bash-completion zsh-completion udev-rules [ udev_files=[ components/engine/contrib/udev ] ]

SUMMARY="ship and run any application as a lightweight container"
DESCRIPTION="
A collaborative project for the container ecosystem to assemble
container-based systems
"
HOMEPAGE="https://mobyproject.org/"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    vim-syntax
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# Some dependencies are from https://github.com/docker/engine/tree/master/hack/dockerfile/install/
# Check each file to know the commit / version of each subproject
DEPENDENCIES="
    build:
        dev-lang/go[>=1.10]
        dev-golang/go-md2man
    build+run:
        dev-db/sqlite:3
        sys-fs/btrfs-progs
        sys-fs/lvm2[>=2.02.103]
        sys-libs/libcap
        sys-libs/libseccomp
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        vim-syntax? ( app-editors/vim )
    run:
        group/docker
        net-firewall/iptables
        net-misc/bridge-utils
        sys-apps/containerd[=1.2.5]
        sys-apps/iproute2
        sys-apps/runc[~1.0.0_rc6_pre20190307]
        sys-apps/tini[=0.18.0]
    post:
        virtualization-lib/libnetwork[=0.8.0_pre20190305] [[
            description = [ provides docker-proxy binary ]
        ]]
"

BUGS_TO="marc-antoine.perennou@clever-cloud.com"

# Testsuite requires to rebuild too and needs a running docker daemon
# We cannot strip because of a runtime check on the docker-init binary
RESTRICT="test strip"

docker_make() {
    DOCKER_CROSSPLATFORMS="linux/386 linux/arm darwin/amd64 darwin/386 freebsd/amd64 freebsd/386freebsd/arm" \
    AUTO_GOPATH=1 \
    IAMSTATIC='false' \
    DOCKER_GITCOMMIT=${MY_PV} \
    VERSION=${MY_PV} \
    edo ./hack/make.sh "$@"
}

# for the cli
GOWORK=src/github.com/docker

src_prepare() {
    default
    edo rm components/engine/contrib/init/systemd/{REVIEWERS,${MY_DPN}.service.rpm} # this isn't a service!
    edo sed -i "s:/usr/bin/:/usr/$(exhost --target)/bin/:" \
        components/engine/contrib/init/systemd/${MY_DPN}.service \
        components/engine/contrib/init/openrc/{${MY_DPN}.confd,${MY_DPN}.initd}

    edo mkdir -p components/cli/${GOWORK}
    edo ln -s ${WORK}/components/cli components/cli/${GOWORK}/cli

    # Docker has multiple man pages systems (one legacy and one new). Both are used to build all the manpages
    # Don't build the bundled version of go-md2man, use the one we package
    # Change the temporary directory for docker's man pages generator binary
    edo sed "/go install/d; s:/tmp/:${TEMP}:g" -i components/cli/scripts/docs/generate-man.sh
}

src_compile() {
    local cli_gopath
    cli_gopath=${WORK}/components/cli

    edo pushd components/engine
    docker_make dynbinary-daemon
    edo popd

    edo pushd components/cli
    LDFLAGS="" GOPATH=${cli_gopath} VERSION=${MY_PV} GITCOMMIT=${MY_PV} edo ./scripts/build/dynbinary
    DISABLE_WARN_OUTSIDE_CONTAINER=1 GOPATH=${cli_gopath} emake manpages
    edo popd
}

src_test() {
    docker_make dyntest
}

src_install() {
    local bindir=/usr/$(exhost --target)/bin

    newbin components/cli/build/${MY_DPN} ${MY_DPN}
    newbin components/engine/bundles/dynbinary-daemon/${MY_DPN}d ${MY_DPN}d
    dosym containerd ${bindir}/docker-containerd
    dosym containerd-shim ${bindir}/docker-containerd-shim
    dosym runc ${bindir}/docker-runc
    dosym ctr ${bindir}/docker-containerd-ctr
    dosym tini-static ${bindir}/docker-init

    install_systemd_files
    install_udev_files
    newinitd components/engine/contrib/init/openrc/docker.initd docker
    newconfd components/engine/contrib/init/openrc/docker.confd docker
    openrc_expart /etc/init.d/docker
    openrc_expart /etc/conf.d/docker
    dobashcompletion components/cli/contrib/completion/bash/docker
    dozshcompletion components/cli/contrib/completion/zsh/_docker
    doman components/cli/man/man{1,5,8}/*

    if option vim-syntax; then
        insinto /usr/share/vim/vimfiles/syntax/
        doins components/engine/contrib/syntax/vim/syntax/dockerfile.vim
        insinto /usr/share/vim/vimfiles/ftdetect/
        doins components/engine/contrib/syntax/vim/ftdetect/dockerfile.vim
    fi

    newbin components/engine/contrib/check-config.sh check-docker-config.sh
}

pkg_postinst() {
    elog "In order to check your kernel/system configuration works with"
    elog "docker, run:"
    elog "  check-docker-config.sh"
}

