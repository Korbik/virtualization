# Copyright 2010 Alex Elsayed <eternaleye@gmail.com>
# Copyright 2014 Vasiliy Tolstov <v.tolstov@selfip.ru>
# Distributed under the terms of the GNU General Public License v2

require bash-completion \
    systemd-service \
    setup-py [ import=setuptools blacklist=3 has_bin=false has_lib=true multibuild=false ]

export_exlib_phases src_configure src_compile src_test src_install

SUMMARY="Open vSwitch is a production quality, multilayer virtual switch"
DESCRIPTION="
Open vSwitch is designed to enable massive network automation through programmatic extension, while
still supporting standard management interfaces (e.g. NetFlow, sFlow, RSPAN, ERSPAN,  CLI).  In
addition, it is designed to support distribution across multiple physical servers similar to
VMware’s vNetwork distributed vswitch or Cisco’s Nexus 1000V.
"
HOMEPAGE="http://openvswitch.org"
DOWNLOADS="${HOMEPAGE}/releases/${PNV}.tar.gz"

BUGS_TO="Alex Elsayed <eternaleye@gmail.com>"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS="
    dkms [[ description = [ Use dkms to build the out-of-tree datapath module. Only used if the upstream one is not built-in ] ]]
    doc
    ipsec [[ description = [ Enables ipsec support ] ]]
    python [[ description = [ Install python bindings ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# have many violations from path/execve sandboxing
RESTRICT="test"

DEPENDENCIES="
    build:
        doc? ( dev-python/Sphinx[python_abis:*(-)?] )
    build+run:
        dev-python/six[python_abis:*(-)?]
        sys-libs/libcap-ng
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    run:
        sys-apps/net-tools
        sys-process/procps
        dkms? ( sys-kernel/dkms )
        ipsec? ( net-misc/ipsec-tools )
        python? (
             dev-python/pyopenssl[python_abis:*(-)?]
             dev-python/zopeinterface[python_abis:*(-)?]
             net-twisted/TwistedConch[python_abis:*(-)?]
        )
    recommendation:
        sys-apps/iproute2 [[ description = [ Support for ingress bandwidth policing ] ]]
    suggestion:
        net-analyzer/tcpdump [[ description = [ More detailed error logs ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-libcapng
    --enable-ndebug
    --enable-shared
    --enable-ssl
    --disable-coverage
    --disable-static
    --disable-Werror
    --with-rundir=/run/openvswitch
    --with-logdir=/var/log/openvswitch
    --with-pkidir=/var/lib/openvswitch/pki
)

openvswitch_src_configure() {
    default

    if option python; then
        edo pushd python
        setup-py_src_configure
        edo popd
    fi
}

openvswitch_src_compile() {
    default

    if option python; then
        edo pushd python
        setup-py_src_compile
        edo popd
    fi
}

openvswitch_src_test() {
    local net allow_nets=(
        "unix:${WORK}/tests/testsuite.dir/*/br*.mgmt"
        "unix:${WORK}/tests/testsuite.dir/*/br*.snoop"
        "unix:${WORK}/tests/testsuite.dir/*/unixctl"
        "unix:${WORK}/tests/testsuite.dir/*/db.sock"
        "unix:${WORK}/tests/testsuite.dir/*/socket"
        "unix:${WORK}/tests/testsuite.dir/*/*/socket"
        "unix:${WORK}/tests/testsuite.dir/*/*.ctl"
        "unix:${WORK}/tests/testsuite.dir/*/socket"
        "unix:${WORK}/tests/testsuite.dir/*/x"
        "unix:${WORK}/tests/testsuite.dir/*/fake-pvconn.0"
        "unix:/tmp/stream-unix.*.*"
        "unix:/tmp/vlog.*.*"
        "unix:/dev/log"
    )

    for net in "${allow_nets[@]}"; do
        esandbox allow_net "${net}"
    done

    default

    for net in "${allow_nets[@]}"; do
        esandbox disallow_net "${net}"
    done
}

openvswitch_src_install() {
    default

    # TODO: check included upstream units in the rhel directory
    install_systemd_files

    if option dkms; then
        insinto /usr/src/${PN}
        hereins dkms.conf <<EOF
PACKAGE_NAME=${PN}
PACKAGE_VERSION=${PV}
BUILT_MODULE_NAME[0]=${PN}
DEST_MODULE_LOCATION[0]="/updates/${PN}"
AUTOINSTALL=yes
EOF

        edo pushd "${WORK}"/datapath/linux
        doins -r .
        edo popd
    fi

    keepdir /var/lib/openvswitch/pki
    keepdir /var/log/openvswitch
    keepdir /etc/openvswitch

    edo rmdir "${IMAGE}"/run/{openvswitch,}

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins openvswitch.conf <<EOF
d /run/openvswitch
EOF

    # Properly install the bash-completions
    dobashcompletion "${IMAGE}"/etc/bash_completion.d/ovs-appctl-bashcomp.bash ovs-appctl
    dobashcompletion "${IMAGE}"/etc/bash_completion.d/ovs-vsctl-bashcomp.bash ovs-vsctl
    edo rm -r "${IMAGE}"/etc/bash_completion.d

    if option doc; then
        insinto /usr/share/doc/${PNVR}
        doins -r "${WORK}"/Documentation/_build/html
    fi

    if option python; then
        edo pushd python
        setup-py_src_install
        edo popd
    fi
}

