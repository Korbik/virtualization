# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Virtual Machine Viewer"
DESCRIPTION="
The 'Virtual Machine Viewer' application (virt-viewer for short package name) is
a lightweight interface for interacting with the graphical display of virtualized
guest OS. It uses GTK-VNC as its display capability, and libvirt to lookup the VNC
server details associated with the guest. It is intended as a replacement for the
traditional vncviewer client, since the latter does not support SSL/TLS encryption
of x509 certificate authentication.
"
HOMEPAGE="http://virt-manager.org"
DOWNLOADS="${HOMEPAGE}/download/sources/${PN}/${PNV}.tar.gz"

BUGS_TO="philantrop@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    (
        gtk-vnc [[ description = [ Use the gtk-vnc viewer ] ]]
        spice [[ description = [ Use the spice-gtk viewer ] ]]
    ) [[ number-selected = at-least-one ]]
    viewer [[ description = [ Only build the remote viewer part without libvirt ] ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
    build+run:
        dev-libs/glib:2[>=2.38.0]
        dev-libs/libxml2[>=2.6.0]
        gnome-platform/libglade[>=2.6.3]
        virtualization-lib/libvirt-glib[>=0.1.8]
        x11-libs/gdk-pixbuf
        x11-libs/gtk+:3[>=3.12]
        x11-libs/pango[>=1.0.0]
        gtk-vnc? ( dev-libs/gtk-vnc:2.0[>=0.4.4][gtk3(+)] )
        spice? (
            virtualization-lib/spice-protocol[>=0.12.7]
            virtualization-ui/spice-gtk:3.0[>=0.35]
        )
        !viewer? ( virtualization-lib/libvirt[>=1.2.8] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-update-mimedb
    --without-ovirt
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    gtk-vnc
    "spice spice-gtk"
    "!viewer libvirt"
)

virt-viewer_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

virt-viewer_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

