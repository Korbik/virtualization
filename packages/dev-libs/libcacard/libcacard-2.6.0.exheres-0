# Copyright 2019 Cédric Corbière
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ] 
require gitlab [ prefix=https://gitlab.freedesktop.org user=spice  ]

SUMMARY="CAC (Common Access Card) library"

DESCRIPTION="
This library provides emulation of smart cards to a virtual card
reader running in a guest virtual machine.

It implements DoD CAC standard with separate pki containers
(compatible with coolkey and OpenSC), using certificates read from NSS.

For more information and API documentation, read the docs/libcacard.txt file.
"

PLATFORMS="~amd64 ~x86"

LICENCES="LGPL"

SLOT="0"

MYOPTIONS="
    pcsc [[ description = [ Enable PCSC support ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/glib:2[>=2.22]
        dev-libs/nss[>=3.12.8]
        pcsc? ( sys-apps/pcsc-lite )
    test:
        dev-libs/pkcs11-helper[nss]
"

UPLOADS_ID="6a765ea4d15b0a70ddcfcf57b7d6c6cd"

DOWNLOADS="https://gitlab.freedesktop.org/spice/${PN}/uploads/${UPLOADS_ID}/${PNV}.tar.xz"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( pcsc )

src_test(){
 echo "Disabled Tests"
}
